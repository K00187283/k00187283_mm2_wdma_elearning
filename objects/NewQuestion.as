﻿package objects {
	
		
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	
	public class NewQuestion extends Sprite {
		

		private var questionText:TextField;	
		
		[Embed(systemFont="Arial Rounded MT Bold", 
			fontName = "myFont", 
			mimeType = "application/x-font", 
			unicodeRange = "U+0020-007E",
			embedAsCFF="false")]
			private var ArialRoundedMTBold:Class;
		
		
		public function NewQuestion() {		
				
			var myFormat:TextFormat = new TextFormat("myFont", 82);
			
			
			questionText = new TextField();
			questionText.embedFonts = true;
			questionText.width = 800;
			questionText.multiline = true;
			questionText.wordWrap = true;
			questionText.autoSize = TextFieldAutoSize.RIGHT;
			questionText.setTextFormat(myFormat);
			questionText.defaultTextFormat = myFormat;
			addChild(questionText);
			
			
		}
		
		
		public function setQuestion(question: String): void {
			
			
			questionText.text = question;
			
			
		}
	}
}
