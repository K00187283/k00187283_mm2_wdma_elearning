﻿package objects {
	
		
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	
	public class NewAnswer extends Sprite {
		

		private var answerText:TextField;	
		public var isCorrect:Boolean = false;
		
		[Embed(systemFont="Arial Rounded MT Bold", 
			fontName = "myFont", 
			mimeType = "application/x-font", 
			unicodeRange = "U+0020-007E",
			embedAsCFF="false")]
			private var ArialRoundedMTBold:Class;
		
		
		public function NewAnswer() {		
				
			var myFormat:TextFormat = new TextFormat("myFont", 82);
			
			
			answerText = new TextField();
			answerText.embedFonts = true;
			answerText.width = 800;
			answerText.multiline = true;
			answerText.wordWrap = true;
			answerText.autoSize = TextFieldAutoSize.RIGHT;
			answerText.setTextFormat(myFormat);
			answerText.defaultTextFormat = myFormat;
			addChild(answerText);
			
			
		}
		
		
		public function setAnswer(answer: String): void {
			
			
			answerText.text = answer;
			
			
		}
	}
}
