﻿package objects {
	
		
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	
	public class Title extends Sprite {
		

		private var titleText:TextField;
		
		[Embed(systemFont="Arial Rounded MT Bold", 
			fontName = "myFont", 
			mimeType = "application/x-font", 
			unicodeRange = "U+0020-007E",
			embedAsCFF="false")]
			private var ArialRoundedMTBold:Class;
		
		
		public function Title() {
				
			var myFormat:TextFormat = new TextFormat("myFont", 30);
			
			
			titleText = new TextField();
			titleText.embedFonts = true;
			titleText.width = 800;
			titleText.multiline = true;
			titleText.wordWrap = true;
			titleText.autoSize = TextFieldAutoSize.RIGHT;
			titleText.setTextFormat(myFormat);
			titleText.defaultTextFormat = myFormat;
			addChild(titleText);
			
			
		}
		
		
		public function setTitle(title: String): void {
			
			
			titleText.text = title;
			
			
		}
	}
}
