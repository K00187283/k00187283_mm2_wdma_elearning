﻿package {
	
	import interfaces.IState;
	import flash.display.Sprite;
	import flash.events.Event;
	import states.Menu;
	import states.Instructions;
	import states.Difficulty;
	import states.Easy;
	import states.Normal;
	import states.Difficult;
	import states.Results;
	
	public class Game extends Sprite {
		
		public static const MENU_STATE:int = 0;
		public static const INSTRUCTIONS_STATE:int = 1;
		public static const DIFFICULTY_STATE:int = 2;
		public static const EASY_STATE:int = 3;
		public static const NORMAL_STATE:int = 4;
		public static const DIFFICULT_STATE:int = 5;
		public static const RESULTS_STATE:int = 6;
		private var current_state:IState;
		
		
		public function Game() {
			
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		
		
		private function init(event:Event):void {
			
			changeState(MENU_STATE);
			
			addEventListener(Event.ENTER_FRAME, update);
			
		}
		
		
		public function changeState(state:int):void {
			
			if(current_state != null) {
				
				current_state.destroy();
				
				current_state = null;
				
			}
			
			
			switch(state) {
				
				case MENU_STATE:
					
					current_state = new Menu(this);
				
					break;
				
				
				case INSTRUCTIONS_STATE:
					
					current_state = new Instructions(this);
				
					break;
				
				
				case DIFFICULTY_STATE:
					
					current_state = new Difficulty(this);
				
					break;
				
				
				case EASY_STATE:
					
					current_state = new Easy(this);
				
					break;
				
				
				case NORMAL_STATE:
					
					current_state = new Normal(this);
				
					break;
				
				
				case DIFFICULT_STATE:
					
					current_state = new Difficult(this);
				
					break;
				
								
				case RESULTS_STATE:
					
					current_state = new Results(this);
				
					break;
				
			}
			
			addChild(Sprite(current_state));
			
		}
		
		
		private function update(event:Event):void {
			
			current_state.update();
		
		}
	}
}