﻿package states {


	import Game;
	import interfaces.IState;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.display.StageScaleMode;
	import flash.media.Sound;
	import flash.media.SoundChannel;


	public class Menu extends Sprite implements IState {


		public var game: Game;
		private var menuBG: MenuBG;
		public var mySong: MySong;
		public var mySoundChannel: SoundChannel;
		private var muteButton: MuteButton;
		private var soundButton: SoundButton;


		public function Menu(game: Game) {

			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}


		private function init(event: Event): void {

			stage.scaleMode = StageScaleMode.EXACT_FIT

			menuBG = new MenuBG();
			addChild(menuBG);
			
			addSounds();

			var difficultyButton: Button = new Button();
			addChild(difficultyButton);
			difficultyButton.height = 80;
			difficultyButton.width = 150;
			difficultyButton.alpha = 0;
			difficultyButton.toggle = true;
			difficultyButton.move(720, 860);
			difficultyButton.addEventListener(MouseEvent.CLICK, onDifficulty);


			var instructionsButton: Button = new Button();
			addChild(instructionsButton);
			instructionsButton.height = 80;
			instructionsButton.width = 150;
			instructionsButton.alpha = 0;
			instructionsButton.toggle = true;
			instructionsButton.move(1035, 860);
			instructionsButton.addEventListener(MouseEvent.CLICK, onInstructions);

		}


		private function addSounds(): void {
			muteButton = new MuteButton;
			muteButton.x = 1800;
			muteButton.y = 15;
			addChild(muteButton);
			muteButton.addEventListener(MouseEvent.CLICK, muteSounds);
		}


		private function muteSounds(e: MouseEvent): void {
			soundButton = new SoundButton;
			mySoundChannel.stop();
			removeChild(muteButton);
			addChild(soundButton);
			soundButton.x = 1800;
			soundButton.y = 15;
			soundButton.addEventListener(MouseEvent.CLICK, playSounds);
		}


		private function playSounds(e: MouseEvent): void {
			removeChild(soundButton);
			playSounds();
		}


		private function onDifficulty(event: Event): void {
			game.changeState(Game.DIFFICULTY_STATE);
		}


		private function onInstructions(event: Event): void {
			game.changeState(Game.INSTRUCTIONS_STATE);
		}


		public function update(): void {}


		public function destroy(): void {
			removeFromParent();
		}


		private function removeFromParent() {
			var child: DisplayObject = this as DisplayObject;
			var parent: DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}