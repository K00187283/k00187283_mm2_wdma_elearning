﻿package states {


	import Game;
	import interfaces.IState;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.text.TextField;
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.display.StageScaleMode;
	import objects.NewQuestion;
	import objects.NewAnswer;
	import objects.Title;




	public class Normal extends Sprite implements IState {


		public var game: Game;
		private var playBG: PlayBG;
		private var newQuestion: NewQuestion;
		private var newAnswer: NewAnswer;
		private var answerArray = new Array;
		public static var score = new Title();
		private var definition = new Title();
		private var correctAnswer = new NewQuestion();
		public static var uScore: Number;
		private var tick: Tick;
		private var ex: Ex;
		private var correct: Correct;
		private var incorrect: Incorrect;
		private var gameState: Number;
		private var exContainerArray: Array = new Array();
		private var exContainer: MovieClip = new MovieClip();
		private var myTimer: Timer = new Timer(1000, 3);


		public function Normal(game: Game) {

			this.game = game;

			gameState = 0;

			uScore = 0;
			updateScoreText();


			myTimer.addEventListener(TimerEvent.TIMER, changeStateTimer);
			addEventListener(Event.ADDED_TO_STAGE, init);

		}

		private function updateScoreText(): void {
			score.setTitle("Score: " + uScore);
			score.x = 435;
			score.y = 750;
			addChild(score);
		}

		private function init(event: Event): void {

			stage.scaleMode = StageScaleMode.EXACT_FIT;

			playBG = new PlayBG;
			addChild(playBG);

			uScore = 0;

			gameState = 0;

			questionOne();

		}





		// Question One
		public function questionOne() {

			updateScoreText();

			var textAnswerArray = new Array("m", "t", "a", "i", "v");
			var correctIndex = 0;
			answerArray = new Array();

			for (var i: int = 0; i < textAnswerArray.length; i++) {
				var tempAnswer: NewAnswer = new NewAnswer();
				tempAnswer.x = 500 + 210 * i;
				tempAnswer.y = 500 + 0 * i;
				tempAnswer.setAnswer(textAnswerArray[i]);
				answerArray.push(tempAnswer);
				addChild(tempAnswer);

				if (i == correctIndex) {
					tempAnswer.isCorrect = true;
					tempAnswer.addEventListener(MouseEvent.CLICK, onRightClick);
				} else {
					tempAnswer.addEventListener(MouseEvent.CLICK, onWrongClick);
				}
			}

			newQuestion = new NewQuestion();
			newQuestion.setQuestion("I_irt");
			newQuestion.x = 870;
			newQuestion.y = 280;
			addChild(newQuestion);

			definition.setTitle("(To play)");
			definition.x = 885;
			definition.y = 400;
			addChild(definition);

		}





		//Question Two
		public function questionTwo() {

			updateScoreText();

			var textAnswerArray = new Array("q", "y", "m", "l", "a");
			var correctIndex = 3;
			answerArray = new Array();

			for (var i: int = 0; i < textAnswerArray.length; i++) {
				var tempAnswer: NewAnswer = new NewAnswer();
				tempAnswer.x = 500 + 210 * i;
				tempAnswer.y = 500 + 0 * i;
				tempAnswer.setAnswer(textAnswerArray[i]);
				answerArray.push(tempAnswer);
				addChild(tempAnswer);


				if (i == correctIndex) {
					tempAnswer.isCorrect = true;
					tempAnswer.addEventListener(MouseEvent.CLICK, onRightClick);
				} else {
					tempAnswer.addEventListener(MouseEvent.CLICK, onWrongClick);
				}
			}


			newQuestion = new NewQuestion;
			newQuestion.setQuestion("So_as");
			newQuestion.x = 850;
			newQuestion.y = 280;
			addChild(newQuestion);

			definition.setTitle("(A light)");
			definition.x = 905;
			definition.y = 400;
			addChild(definition);
		}





		//Question Three
		public function questionThree() {

			updateScoreText();

			var textAnswerArray = new Array("j", "o", "u", "s", "n");
			var correctIndex = 2;
			answerArray = new Array();

			for (var i: int = 0; i < textAnswerArray.length; i++) {
				var tempAnswer: NewAnswer = new NewAnswer();
				tempAnswer.x = 500 + 210 * i;
				tempAnswer.y = 500 + 0 * i;
				tempAnswer.setAnswer(textAnswerArray[i]);
				answerArray.push(tempAnswer);
				addChild(tempAnswer);


				if (i == correctIndex) {
					tempAnswer.isCorrect = true;
					tempAnswer.addEventListener(MouseEvent.CLICK, onRightClick);
				} else {
					tempAnswer.addEventListener(MouseEvent.CLICK, onWrongClick);
				}
			}


			newQuestion = new NewQuestion;
			newQuestion.setQuestion("_rlar");
			newQuestion.x = 870;
			newQuestion.y = 280;
			addChild(newQuestion);

			definition.setTitle("(The floor)");
			definition.x = 880;
			definition.y = 400;
			addChild(definition);
		}




		//Question Four
		public function questionFour() {

			updateScoreText();

			var textAnswerArray = new Array("i", "t", "o", "d", "f");
			var correctIndex = 1;
			answerArray = new Array();

			for (var i: int = 0; i < textAnswerArray.length; i++) {
				var tempAnswer: NewAnswer = new NewAnswer();
				tempAnswer.x = 500 + 210 * i;
				tempAnswer.y = 500 + 0 * i;
				tempAnswer.setAnswer(textAnswerArray[i]);
				answerArray.push(tempAnswer);
				addChild(tempAnswer);


				if (i == correctIndex) {
					tempAnswer.isCorrect = true;
					tempAnswer.addEventListener(MouseEvent.CLICK, onRightClick);
				} else {
					tempAnswer.addEventListener(MouseEvent.CLICK, onWrongClick);
				}
			}


			newQuestion = new NewQuestion;
			newQuestion.setQuestion("Ma_hair");
			newQuestion.x = 800;
			newQuestion.y = 280;
			addChild(newQuestion);

			definition.setTitle("(Mother)");
			definition.x = 890;
			definition.y = 400;
			addChild(definition);
		}




		//Question Five
		public function questionFive() {

			updateScoreText();

			var textAnswerArray = new Array("d", "t", "a", "l", "c");
			var correctIndex = 4;
			answerArray = new Array();

			for (var i: int = 0; i < textAnswerArray.length; i++) {
				var tempAnswer: NewAnswer = new NewAnswer();
				tempAnswer.x = 500 + 210 * i;
				tempAnswer.y = 500 + 0 * i;
				tempAnswer.setAnswer(textAnswerArray[i]);
				answerArray.push(tempAnswer);
				addChild(tempAnswer);


				if (i == correctIndex) {
					tempAnswer.isCorrect = true;
					tempAnswer.addEventListener(MouseEvent.CLICK, onRightClick);
				} else {
					tempAnswer.addEventListener(MouseEvent.CLICK, onWrongClick);
				}
			}


			newQuestion = new NewQuestion;
			newQuestion.setQuestion("Tea_h");
			newQuestion.x = 840;
			newQuestion.y = 280;
			addChild(newQuestion);

			definition.setTitle("(House)");
			definition.x = 900;
			definition.y = 400;
			addChild(definition);
		}





		public function changeGameState(): void {

			gameState++;

			switch (gameState) {

				case 1:
					myTimer.reset();
					removeChild(newQuestion);
					removeChild(definition);
					removeChild(tick);


					while (exContainer.numChildren > 0) {
						exContainer.removeChildAt(0);
					}


					for (var i: int = 0; i < answerArray.length; i++) {
						removeChild(answerArray[i]);
					}


					questionTwo();
					break;


				case 2:
					myTimer.reset();
					removeChild(newQuestion);
					removeChild(definition);
					removeChild(tick);


					while (exContainer.numChildren > 0) {
						exContainer.removeChildAt(0);
					}


					for (i = 0; i < answerArray.length; i++) {
						removeChild(answerArray[i]);
					}


					questionThree();
					break;


				case 3:
					myTimer.reset();
					removeChild(newQuestion);
					removeChild(definition);
					removeChild(tick);


					while (exContainer.numChildren > 0) {
						exContainer.removeChildAt(0);
					}


					for (i = 0; i < answerArray.length; i++) {
						removeChild(answerArray[i]);
					}


					questionFour();
					break;


				case 4:
					myTimer.reset();
					removeChild(newQuestion);
					removeChild(definition);
					removeChild(tick);


					while (exContainer.numChildren > 0) {
						exContainer.removeChildAt(0);
					}


					for (i = 0; i < answerArray.length; i++) {
						removeChild(answerArray[i]);
					}


					questionFive();
					break;

				case 5:
					myTimer.reset();
					removeChild(newQuestion);
					removeChild(definition);
					removeChild(tick);


					while (exContainer.numChildren > 0) {
						exContainer.removeChildAt(0);
					}


					for (i = 0; i < answerArray.length; i++) {
						removeChild(answerArray[i]);
					}

					game.changeState(Game.RESULTS_STATE);

					gameState = 0;
			}
		}





		private function onRightClick(e: MouseEvent): void {
			//incrementScore
			uScore += 10;

			//addTick
			tick = new Tick;
			addChild(tick);


			//changeAnswer
			if (gameState == 0) {
				newQuestion.setQuestion("Imirt");
				tick.x = newQuestion.x + 240;
				tick.y = 250;
			} else if (gameState == 1) {
				newQuestion.setQuestion("Solas");
				tick.x = newQuestion.x + 250;
				tick.y = 250;
			} else if (gameState == 2) {
				newQuestion.setQuestion("Urlar");
				tick.x = newQuestion.x + 250;
				tick.y = 250;
			} else if (gameState == 3) {
				newQuestion.setQuestion("Mathair");
				tick.x = newQuestion.x + 350;
				tick.y = 250;
			} else if (gameState == 4) {
				newQuestion.setQuestion("Teach");
				tick.x = newQuestion.x + 280;
				tick.y = 250;
			}

			myTimer.start();


			//addSound
			correct = new Correct;
			correct.play();
		}





		private function onWrongClick(e: MouseEvent): void {
			uScore -= 2;

			exContainer.name = "exContainer";
			addChild(exContainer);


			//addEx to Container
			for (var i: int = 0; i <= 4; i++) {
				ex = new Ex;
				exContainerArray.push(ex);
				exContainer.addChild(ex);
				ex.x = e.stageX;
				ex.y = e.stageY;
				e.target.alpha = 0;
			}


			//addSound
			var incorrect = new Incorrect;
			incorrect.play();
		}


		public function changeStateTimer(e: TimerEvent): void {
			if (myTimer.repeatCount == 3) {
				changeGameState();
			}
		}


		private function onResults(event: Event): void {

			game.changeState(Game.RESULTS_STATE);

		}

		private function onMenu(event: Event): void {

			game.changeState(Game.MENU_STATE);

		}

		public function update(): void {}


		public function destroy(): void {
			removeEventListener(Event.COMPLETE, onResults);
			removeEventListener(MouseEvent.CLICK, onRightClick);
			removeEventListener(MouseEvent.CLICK, onWrongClick);

			playBG = null;
			removeFromParent();

		}

		private function removeFromParent() {

			var child: DisplayObject = this as DisplayObject;
			var parent: DisplayObjectContainer = child.parent;

			parent.removeChild(child);

		}
	}
}