﻿package states {


	import Game;
	import interfaces.IState;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import flash.display.StageScaleMode;


	public class Instructions extends Sprite implements IState {


		public var game: Game;
		public var instructionsBG: InstructionsBG;


		public function Instructions(game: Game) {

			this.game = game;

			addEventListener(Event.ADDED_TO_STAGE, init);

		}

		private function init(event: Event): void {

			stage.scaleMode = StageScaleMode.EXACT_FIT;

			instructionsBG = new InstructionsBG();
			addChild(instructionsBG);

			var difficultyButton: Button = new Button();
			addChild(difficultyButton);
			difficultyButton.height = 80;
			difficultyButton.width = 150;
			difficultyButton.alpha = 0;
			difficultyButton.toggle = true;
			difficultyButton.move(720, 860);
			difficultyButton.addEventListener(MouseEvent.CLICK, onDifficulty);


			var backButton: Button = new Button();
			addChild(backButton);
			backButton.height = 80;
			backButton.width = 150;
			backButton.alpha = 0;
			backButton.toggle = true;
			backButton.move(1035, 860);
			backButton.addEventListener(MouseEvent.CLICK, onMenu);

		}

		private function onDifficulty(event: Event): void {

			game.changeState(Game.DIFFICULTY_STATE);

		}

		private function onMenu(event: Event): void {

			game.changeState(Game.MENU_STATE);

		}

		public function update(): void {}

		public function destroy(): void {
			removeFromParent();
		}

		private function removeFromParent() {

			var child: DisplayObject = this as DisplayObject;
			var parent: DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}