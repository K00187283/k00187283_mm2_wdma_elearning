﻿package states {


	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.display.StageScaleMode;
	import states.Easy;
	import states.Normal;
	import states.Difficult;
	import objects.Title;
	import objects.NewQuestion;
	import interfaces.IState;




	public class Results extends Sprite implements IState {

		private var resultsBG: ResultsBG;
		public static var uScore: Number;
		public static var score = new Title();
		private var recommendation = new Title();
		public var game: Game;


		public function Results(game: Game) {

			this.game = game;

			addEventListener(Event.ADDED_TO_STAGE, init);

		}

		private function init(event: Event): void {

			stage.scaleMode = StageScaleMode.EXACT_FIT;

			resultsBG = new ResultsBG;
			addChild(resultsBG);

			updateScoreText();

			displayRecommendation();


			var replayButton: Button = new Button();
			addChild(replayButton);
			replayButton.height = 80;
			replayButton.width = 150;
			replayButton.alpha = 0;
			replayButton.toggle = true;
			replayButton.move(720, 860);
			replayButton.addEventListener(MouseEvent.CLICK, onReplay);

			var menuButton: Button = new Button();
			addChild(menuButton);
			menuButton.height = 80;
			menuButton.width = 150;
			menuButton.alpha = 0;
			menuButton.toggle = true;
			menuButton.move(1035, 860);
			menuButton.addEventListener(MouseEvent.CLICK, onMenu);

		}

		private function updateScoreText(): void {
			score.setTitle("Score: " + uScore);

			addChild(Easy.score);
			Easy.score.x = 880;
			Easy.score.y = 405;
			addChild(Normal.score);
			Normal.score.x = 880;
			Normal.score.y = 405;
			addChild(Difficult.score);
			Difficult.score.x = 880;
			Difficult.score.y = 405;

		}


		private function displayRecommendation(): void {
			recommendation = new Title();

			if (Easy.uScore <= 20) {
				recommendation.setTitle("You need more practice, try again.");
				addChild(recommendation);
				recommendation.x = 705;
				recommendation.y = 620;
			} else {
				recommendation.setTitle("Good job move on to a new difficulty, if possible or play again");
				addChild(recommendation);
				recommendation.x = 550;
				recommendation.y = 590;
			}


			if (Normal.uScore <= 20) {
				recommendation.setTitle("You need more practice, try again.");
				addChild(recommendation);
				recommendation.x = 705;
				recommendation.y = 620;
			} else {
				recommendation.setTitle("Good job move on to a new difficulty, if possible or play again");
				addChild(recommendation);
				recommendation.x = 550;
				recommendation.y = 590;
			}


			if (Difficult.uScore <= 20) {
				recommendation.setTitle("You need more practice, try again.");
				addChild(recommendation);
				recommendation.x = 705;
				recommendation.y = 620;
			} else {
				recommendation.setTitle("Good job move on to a new difficulty, if possible or play again");
				addChild(recommendation);
				recommendation.x = 550;
				recommendation.y = 590;
			}


		}

		private function onReplay(event: Event): void {

			game.changeState(Game.DIFFICULTY_STATE);

		}

		private function onMenu(event: Event): void {

			game.changeState(Game.MENU_STATE);

		}

		public function update(): void {


		}

		public function destroy(): void {

			removeFromParent();

		}

		private function removeFromParent() {

			var child: DisplayObject = this as DisplayObject;
			var parent: DisplayObjectContainer = child.parent;

			parent.removeChild(child);

		}
	}
}