﻿package states {


	import Game;
	import interfaces.IState;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import flash.display.StageScaleMode;


	public class Difficulty extends Sprite implements IState {


		public var game: Game;
		public var difficultyBG: DifficultyBG;


		public function Difficulty(game: Game) {
			this.game = game;

			addEventListener(Event.ADDED_TO_STAGE, init);

		}

		private function init(event: Event): void {

			stage.scaleMode = StageScaleMode.EXACT_FIT;

			difficultyBG = new DifficultyBG();
			addChild(difficultyBG);

			var easyButton: Button = new Button();
			addChild(easyButton);
			easyButton.height = 63;
			easyButton.width = 456;
			easyButton.alpha = 0;
			easyButton.toggle = true;
			easyButton.move(700, 380);
			easyButton.addEventListener(MouseEvent.CLICK, onEasy);


			var normalButton: Button = new Button();
			addChild(normalButton);
			normalButton.height = 63;
			normalButton.width = 503;
			normalButton.alpha = 0;
			normalButton.toggle = true;
			normalButton.move(685, 520);
			normalButton.addEventListener(MouseEvent.CLICK, onNormal);


			var difficultButton: Button = new Button();
			addChild(difficultButton);
			difficultButton.height = 63;
			difficultButton.width = 560;
			difficultButton.alpha = 0;
			difficultButton.toggle = true;
			difficultButton.move(670, 665);
			difficultButton.addEventListener(MouseEvent.CLICK, onDifficult);

		}

		private function onEasy(event: Event): void {

			game.changeState(Game.EASY_STATE);

		}

		private function onNormal(event: Event): void {

			game.changeState(Game.NORMAL_STATE);

		}

		private function onDifficult(event: Event): void {

			game.changeState(Game.DIFFICULT_STATE);

		}


		public function update(): void {}

		public function destroy(): void {
			removeFromParent();
		}

		private function removeFromParent() {

			var child: DisplayObject = this as DisplayObject;
			var parent: DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}